package com.cts.proj.onlineshopping;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.cts.project.model.Product;

@Configuration
@ComponentScan("com.cts.project")
public class AppInIt {

}
